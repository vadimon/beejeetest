<?php

// 1.show errors
ini_set('display_errors', 1);
error_reporting(E_ALL);


// 2. Connecting system files
define('ROOT', dirname(__FILE__));

require_once(ROOT . '/../vendor/autoload.php');

/**
 * session start
 */
\Components\Session::my_session_start();

/**
 * Router
 */
$router = new \Components\Router();
$router->run();
