<?php

namespace Components;


class Router
{
    private $routes;

    public function __construct()
    {
        $routesPath = ROOT.'/../config/routes.php';
        $this->routes = include ($routesPath);
    }


    /**
     * Returns request string
     */
    private function getURL()
    {
        $uri = '';

        if (!empty($_SERVER['REQUEST_URI'])) {
            $uri = $_SERVER['REQUEST_URI'];
        }

        if (($cutoff = strpos($uri, '?')) !== false) {
            $uri = substr($uri, 0, $cutoff);
        }

        return trim($uri, '/');
    }



    public function run()
    {
        $url = $this->getURL();

        foreach ($this->routes as $urlPattern => $path){

            if (preg_match("~$urlPattern~", $url)) {

                $internalRoute = preg_replace("`$urlPattern`", $path, $url);

                $segments = explode('/', $internalRoute);

                $controllerName = array_shift($segments).'Controller';

                $controllerName = ucfirst($controllerName);

                $actionName = 'action' . ucfirst(array_shift($segments));

                $parameters = $segments;

                $controllerClassName = '\\Controllers\\' . $controllerName;


                $controllerObject =  new $controllerClassName;

                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                if ($result != null){
                    break;
                }
            }
        }
    }
}
