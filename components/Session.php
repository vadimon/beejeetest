<?php


namespace Components;


class Session
{



    /**
     * start session
     */
    public static function my_session_start()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    /**
     * set value in session
     * @param $key
     * @param $val
     */
    public static function my_session_set($key, $val)
    {
        $_SESSION[$key] = $val;
    }


    /**
     * get data from session
     * @param $key
     * @return mixed
     */
    public static function my_session_get($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
        else {
            return false;
        }
    }

    /**
     * delete session
     * @param $key
     */
    public static function my_session_clear($key)
    {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }

    /**
     * set value in flash-session
     * @param $key
     * @param $val
     */
    public static function my_session_flash_set($key, $val)
    {
        $_SESSION['_flash'][$key] = $val;
    }

    /**
     * get data from flash-session
     * @param $key
     * @return false|mixed
     */
    public static function my_session_flash_get($key)
    {
        if (isset($_SESSION['_flash'][$key])) {
            $data = $_SESSION['_flash'][$key];
            unset($_SESSION['_flash'][$key]);

            return $data;
        }
        else {
            return false;
        }
    }

}