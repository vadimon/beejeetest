<?php

namespace Controllers;


use Components\Session;
use Models\Task;
use Models\User;
use Valitron\Validator;

class MainController extends Controller
{

    /**
     * load main page
     * @return mixed
     */
    public function actionIndex()
    {
        $perPage = $this->perPage;
        $page = $this->pagination()['page'];
        $startAt = $this->pagination()['start_page'];
        $sort = $this->pagination()['sort'];
        $order = $this->pagination()['order'];

        $existUser = Session::my_session_flash_get('is_logged_user');
        $logout = Session::my_session_flash_get('logout');
        $result = Task::all($perPage,$startAt,$sort,$order);
        $taskList = $result['tasks'];
        $pages = $result['pages'];

        require_once(ROOT . '/../view/index.php');
        return true;
    }

    /**
     * show login page
     * @return bool
     */
    public function actionLogin()
    {
        $login_error = Session::my_session_flash_get('login_error');
        $error_login_user = Session::my_session_flash_get('error_user');

        require_once(ROOT. '/../view/login.php');

        return true;
    }


    /**
     * get data from login form
     * @return bool
     */
    public function actionLoging()
    {
        $this->authUser();
        $this->checkMethod();
        if (!empty($_POST)) {
            $validationData = $this->validateLoginData($_POST);
            $result = $this->checkAuth($validationData);
            if (!$result) {
                Session::my_session_flash_set('error_user','User '.$validationData['name']. 'is not registered');
                header("Location:/login");
            } else {
                header("Location:/");
            }
        }

        return true;
    }

    /**
     * check if logged user
     * @param $data
     * @return bool
     */
    private function checkAuth($data)
    {
        //var_dump($data);
        $user = User::authUser($data['name'], $data['password']);
        var_dump($user);
        if ($user) {
            Session::my_session_set('user', $user);
            return true;
        }
        return false;
    }

    /**
     * validation data
     * @param $data
     * @return array|false
     */
    private function validateLoginData($data)
    {
        $validation = new Validator(array(
            'login' => $data['login'],
            'password' => $data['password']
        ));
        $validation->rule('required', ['login', 'password'])->message('{field} is required');
        $validation->labels([
            'login' => 'Login',
            'password' => 'Password'
        ]);
        if ($validation->validate()) {
            return [
                'name' => htmlspecialchars($data['login']),
                'password' => htmlspecialchars($data['password']),
            ];

        } else {
             Session::my_session_flash_set('login_error', $validation->errors());
            header('Location:/login');
        }
    }

    /**
     * check request method , if post go next else return 404 error
     */
    private function checkMethod()
    {
        if ($_SERVER['REQUEST_METHOD'] !== REQUEST_POST) {
            header("HTTP/1.0 404 Not Found");
            echo "<h1>404 Not Found</h1>";
            echo "The page that you have requested could not be found.";
            exit();
        }

    }

    /**
     *
     */
    private function authUser()
    {
        if (Session::my_session_get('user')) {
            Session::my_session_flash_set('is_logged_user','You already logged');
            header("Location:/");
        }
    }

    /**
     * logout user
     * @return bool
     */
    public function actionLogout()
    {
        Session::my_session_clear('user');
        Session::my_session_flash_set('logout', 'You logout successful');

        header('Location:/');
        return true;
    }



}