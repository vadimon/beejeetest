<?php


namespace Controllers;


use Components\Session;

class Controller
{

    protected $perPage = 3;

    /**
     * @return array
     */
    protected function pagination()
    {
        $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
        $sort = (isset($_GET['sort'])) ? $_GET['sort'] : null;
        $order = (isset($_GET['order'])) ? $_GET['order'] : null;
        $startAt = $this->perPage * ($page - 1);

        return [
            'page' => $page ,
            'start_page' => $startAt,
            'sort' => $sort,
            'order' => $order
        ];

    }

    /**
     * check if auth user
     */
    protected function onlyAuth()
    {
        if (!Session::my_session_get('user')) {
            Session::my_session_flash_set('error_user', 'You must log in');
            header('Location:/login');
        }
    }
}