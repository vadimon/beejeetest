<?php


namespace Controllers;


use Components\Session;
use Models\Task;
use Valitron\Validator;

class TaskController extends Controller
{


    /**
     * show create task page
     * @return bool
     */
    public function actionCreate()
    {
        $error_create = Session::my_session_flash_get('create_task_error');
        $success_create = Session::my_session_flash_get('success_create');

        require_once(ROOT . '/../view/create_task.php');

        return true;
    }

    /**
     * save task to DB
     */
    public function actionStore()
    {
        if ($validationData = $this->validation($_POST)) {
            if (Task::save($validationData)) {
                Session::my_session_flash_set('success_create', 'Task successfully create');
            }
        }

        header('Location:create');

        return true;
    }

    /**
     * validation data for save task
     * @param $data
     * @return array|false
     */
    private function validation($data)
    {
        $v = new Validator($data);
        $v->rule('required',['name', 'email', 'task']);
        $v->rule('email','email');
        $v->labels([
            'name' => 'Your Name',
            'email' => 'Email address',
            'task' => 'Task',
            'status' => 'Status'
        ]);
        if ($v->validate()) {
            return [
                'name' => htmlspecialchars($data['name']),
                'email' => htmlspecialchars($data['email']),
                'task' => htmlspecialchars($data['task']),
            ];

        } else {
            Session::my_session_flash_set('create_task_error', $v->errors());
            return false;
        }
    }

    /**
     * show edit page
     * @param $id
     * @return bool
     */
    public function actionEdit($id)
    {
        $this->onlyAuth();
        $task = Task::find($id);
        $update_error = Session::my_session_flash_get('update_task_error');
        require_once(ROOT.'/../view/edit.php');
        return true;
    }


    /**
     * @param $id
     * @return bool
     */
    public function actionUpdate($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == REQUEST_GET) {
            Task::update($id,'status','done');
            header('Location:/');
        } elseif ($_SERVER['REQUEST_METHOD'] == REQUEST_POST) {
            $data = $this->validUpdate($_POST);
            Task::update($id,'task', $data, '1');

            header('Location:/edit/'.$id);
        }

        return true;
    }

    /**
     * validation edit task
     * @param $data
     * @return false|string
     */
    private function validUpdate($data)
    {
        $v = new Validator($data);
        $v->rule('required', 'task')->label('Task');
        if ($v->validate()) {
            return htmlspecialchars($data['task']);

        } else {
            Session::my_session_flash_set('update_task_error', $v->errors());
            return false;
        }
    }



}