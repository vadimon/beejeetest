<?php
require_once(ROOT . '/../view/layouts/header.php');
?>
    <div class="container">

        <div class="d-flex justify-content-center col-10">

            <form method="POST" action="/update/<?php echo $task['id'];?>">
                <div class="form-group">
                    <label>User name</label>
                    <input type="text" class="form-control" name="name" value="<?php echo $task['user_name']?>" disabled required>
                </div>

                <div class="form-group">
                    <label>Email address</label>
                    <input type="email" class="form-control" name="email" value="<?php echo $task['email']?>" disabled required>

                </div>

                <div class="form-group">
                    <label >Task</label>
                    <textarea name="task" class="form-control" ><?php echo $task['task']?></textarea>

                    <?php if(isset($update_error['task'])):?>
                        <?php foreach ($update_error['task'] as $error):?>
                            <div class="alert alert-danger" role="alert">
                                <strong><?php echo $error?></strong>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>

                </div>

                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>


<?php require_once (ROOT . '/../view/layouts/footer.php');
