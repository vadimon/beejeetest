<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8">
    <link rel="icon" href="../../public/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <title>Task</title>
</head>
<body>
<div class="container">
    <header>
        <nav class="navbar navbar-light bg-light">
            <a href="/" class="navbar-brand">Main</a>
            <?php if(\Components\Session::my_session_get('user'))  :?>
                <a href="logout" class="btn btn-outline-primary my-2 my-sm-0">Logout</a>
            <?php else :?>
                <a href="login" class="btn btn-outline-primary my-2 my-sm-0">Login</a>
            <?php endif;?>
        </nav>

    </header>
