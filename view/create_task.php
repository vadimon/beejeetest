<?php
require_once(ROOT . '/../view/layouts/header.php');
?>
    <div class="container">
        <?php if($success_create) :?>
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                        <?php echo $success_create?>
                    </div>
                </div>
            </div>
        <?php endif;?>
        <div class="d-flex justify-content-center col-8">

            <form method="POST" action="/store">
                <div class="form-group">
                    <label>Your name</label>
                    <input type="text" class="form-control" name="name" required>

                    <?php if(isset($error_create['name'])):?>
                        <?php foreach ($error_create['name'] as $error):?>
                            <div class="alert alert-danger" role="alert">
                                <strong><?php echo $error?></strong>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>

                </div>

                <div class="form-group">
                    <label>Email address</label>
                    <input type="email" class="form-control" name="email" required>

                    <?php if(isset($error_create['email'])):?>
                            <?php foreach ($error_create['email'] as $error):?>
                                <div class="alert alert-danger" role="alert">
                                    <strong><?php echo $error?></strong>
                                </div>
                            <?php endforeach;?>
                    <?php endif;?>

                </div>

                <div class="form-group">
                    <label >Task</label>
                    <textarea name="task" class="form-control" required></textarea>

                    <?php if(isset($error_create['task'])):?>
                        <?php foreach ($error_create['task'] as $error):?>
                            <div class="alert alert-danger" role="alert">
                                <strong><?php echo $error?></strong>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>

                </div>

                <button type="submit" class="btn btn-primary">Create</button>
            </form>
        </div>
    </div>


<?php require_once (ROOT . '/../view/layouts/footer.php');
