<?php
require_once(ROOT . '/../view/layouts/header.php');
?>
<div class="container">
    <?php if($error_login_user) :?>
        <div class="row justify-content-center">
            <div class="col-md-3">
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <?php echo $error_login_user?>
                </div>
            </div>
        </div>
    <?php endif;?>
    <div class="d-flex justify-content-center col-8">

        <form method="POST" action="/loging">
            <div class="form-group">
                <label>Login</label>
                <input type="text" class="form-control" name="login" >
                <small  class="form-text text-muted">We'll never share your email with anyone else.</small>

                <?php if(isset($login_error['login'])):?>
                    <?php foreach ($login_error['login'] as $error):?>
                        <div class="alert alert-danger" role="alert">
                            <strong><?php echo $error?></strong>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>

            </div>
            <div class="form-group">
                <label >Password</label>
                <input type="password" name="password" class="form-control" >

                <?php if(isset($login_error['password'])):?>
                    <?php foreach ($login_error['password'] as $error):?>
                        <div class="alert alert-danger" role="alert">
                            <strong><?php echo $error?></strong>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>

            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>


<?php require_once (ROOT . '/../view/layouts/footer.php');
