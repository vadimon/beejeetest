<?php
require_once(ROOT . '/../view/layouts/header.php');
?>
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">All Task</li>
            </ol>
        </nav>

        <?php if($existUser) :?>
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                        <?php echo $existUser?>
                    </div>
                </div>
            </div>
        <?php endif;?>

        <?php if($logout) :?>
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                        <?php echo $logout?>
                    </div>
                </div>
            </div>
        <?php endif;?>


        <a href="/create" class="btn btn-outline-primary btn-sm mb-2">Create Task</a>

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">User Name
                    <a href="?sort=user_name&order=ASC&page=<?php echo $page?>"><i class="fa fa-fw fa-sort-asc"></i></a>
                    <a href="?sort=user_name&order=DESC&page=<?php echo $page?>"><i class="fa fa-fw fa-sort-desc"></i></a>
                </th>
                <th scope="col">Email
                    <a href="?sort=email&order=ASC&page=<?php echo $page?>"><i class="fa fa-fw fa-sort-asc"></i></a>
                    <a href="?sort=email&order=DESC&page=<?php echo $page?>"><i class="fa fa-fw fa-sort-desc"></i></a>
                </th>
                <th scope="col">Task </th>
                <th scope="col">Edit </th>
                <th scope="col">Status
                    <a href="?sort=status&order=ASC&page=<?php echo $page?>"><i class="fa fa-fw fa-sort-asc"></i></a>
                    <a href="?sort=status&order=DESC&page=<?php echo $page?>"><i class="fa fa-fw fa-sort-desc"></i></a>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($taskList as $key => $task) :?>
                <tr>
                    <th scope="row"><?php echo $key + 1;?></th>
                    <td><?php echo $task['user_name'];?></td>
                    <td><?php echo $task['email'];?></td>
                    <td>
                        <?php echo $task['task']; ?>
                        <?php if (\Components\Session::my_session_get('user')) :?>
                            <a href="/edit/<?php echo $task['id'];?>" class="btn btn-sm btn-outline-primary">edit</a>
                        <?php endif ?>
                    </td>
                    <td>
                        <?php if ($task['edit']): ?>
                            <?php echo 'edited by admin'?>
                        <?php endif ;?>
                    </td>
                    <td>
                        <?php echo $task['status'];?>
                        <?php if (\Components\Session::my_session_get('user') && $task['status'] !== 'done') :?>
                            <a href="/update/<?php echo $task['id'];?>" class="btn btn-sm btn-outline-primary">done</a>
                        <?php endif ?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>

        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="mr-2"><a href="?page=1">First</a></li>
                <li class="<?php if($page <= 1){ echo 'disabled'; } ?> mr-2">
                    <a href="<?php if($page <= 1){ echo '#'; } else { echo "?sort=$sort&order=$order&page=".($page - 1); } ?>">Prev</a>
                </li>
                <li class="<?php if($page >= $pages){ echo 'disabled'; } ?> mr-2">
                    <a href="<?php if($page >= $pages){ echo '#'; } else { echo "?sort=$sort&order=$order&page=".($page + 1); } ?>">Next</a>
                </li>
                <li><a href="?page=<?php echo $pages; ?>">Last</a></li>

            </ul>
        </nav>

    </div>
<?php require_once (ROOT . '/../view/layouts/footer.php');
