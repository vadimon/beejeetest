<?php
return array(
    'edit/([0-9]+)' => 'task/edit/$1', // actionDone in TaskController
    'update/([0-9]+)' => 'task/update/$1', // actionDone in TaskController
    'store' => 'task/store', // actionStore in TaskController
    'create' => 'task/create', // actionCreate in TaskController
    'logout' => 'main/logout', //actionLogin in MainController
    'loging' => 'main/loging', //actionLogin in MainController
    'login' => 'main/login', //actionLogin in MainController
    //'/[\?](page)=([0-9]+)/' => 'main/index', //actionIndex in MainController
    '/[\?&]([^&]+)=([^&]+)/' => 'main/index', //actionIndex in MainController
    '' => 'main/index', //actionIndex in MainController
);
