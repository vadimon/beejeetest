<?php


namespace Models;


use Components\Db;
use PDO;

class Task
{

    /**
     * @param $perPage
     * @param $startAt
     * @param string $sort
     * @param string $order
     * @return array
     */
    public static function all($perPage,$startAt,$sort = null,$order = null)
    {
        $db = Db::getConnection();

        $sql_tasks = 'SELECT * FROM tasks ORDER BY '. ($sort ? $sort : 'id') .' '. ($order ? $order : ' DESC') . ' LIMIT '. $startAt .','. $perPage;
        $tasks = $db->prepare($sql_tasks);
        $tasks->execute();
        $res_tasks = $tasks->fetchAll(PDO::FETCH_ASSOC);

        return $result = [
            'pages' => self::pagination($perPage),
            'tasks' => $res_tasks
        ];

    }

    /**
     * save task
     * @param $data
     * @return bool
     */
    public static function save($data)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO tasks (user_name, email, task) 
                VALUE (:name, :email, :task)';
        $task = $db->prepare($sql);
        $task->bindParam(':name', $data['name']);
        $task->bindParam(':email', $data['email']);
        $task->bindParam(':task', $data['task']);
        return $task->execute();
    }

    /**
     * @param $perPage
     * @return false|float
     */
    private static function pagination($perPage)
    {
        $db = Db::getConnection();

        $sql_count = "SELECT COUNT(*) FROM tasks";
        $rows = $db->prepare($sql_count);
        $rows->execute();
        $count = $rows->fetch(PDO::FETCH_ASSOC)['COUNT(*)'];

        return ceil($count / $perPage);
    }

    /**
     * @param $id
     * @return bool
     */
    public static function update($id,$fild,$param,$edit = '0')
    {
        $db = Db::getConnection();
        $sql = 'UPDATE tasks SET '.$fild .'=:value, edit=:edit WHERE id=:id';
        $task = $db->prepare($sql);
        $task->bindParam(':id', $id);
        $task->bindParam(':value', $param);
        $task->bindParam(':edit', $edit);
        return $task->execute();
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function find($id)
    {
        $db = Db::getConnection();
        $sql = "SELECT * FROM tasks WHERE id =:id";
        $task = $db->prepare($sql);
        $task->bindParam(':id', $id);
        $task->execute();
        return $task->fetch(PDO::FETCH_ASSOC);
    }



}