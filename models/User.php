<?php


namespace models;


use Components\Db;
use PDO;

class User
{

    public static function authUser($login, $password)
    {
        $db = Db::getConnection();
        $sql = "SELECT name, password FROM users WHERE name=:name AND password=:password";
        $user = $db->prepare($sql);
        $user->bindParam(':name', $login);
        $user->bindParam(':password', $password);
        $user->execute();

        return $user->fetch(PDO::FETCH_ASSOC);
    }

}